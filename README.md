# Read Litepoint IQVSA

Litepoint equipments are useful in design validation and device characterizations. Older modles used Matlab .mat format files to store waveforms,
but newer models moved away from it. 

The functions here provide a simple way to extract the data from iqvsa files so we can run analyses on the data being captured by Litepoint VSA.