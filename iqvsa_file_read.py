import numpy as np
import xml.etree.ElementTree as ET
import array
import sys

def main():
    if len(sys.argv) != 2:
        print('Usage: {} filename'.format(sys.argv[0]))
    else:
        wave, info, config = iqvsa_read(sys.argv[1])
    return

def extract_data(byte_values):
    data = np.array([])
    info = {}
    enc = True
    header_tail = '</LPHeader>'.encode()
    header_tail_pos = byte_values.find(header_tail)+len(header_tail)
    header = byte_values[:header_tail_pos].decode()
    header_root = ET.fromstring(header)
    element = header_root.find('General').find('Encoding')
    if element == None:
        enc = False
    elif element.text == 'False':
        enc = False
    infolist = (('General','Encoding'), ('DataInfo','SamplingRate'), ('DataInfo','SampleCount'), ('DataInfo','Timestamp'),\
                ('DataInfo','TimestampClockRate'),  ('DataInfo','Integrity'))
    for item in infolist:
        element = header_root.find(item[0]).find(item[1])
        if element != None:
            try:
                info[item[1]] = int(element.text)
            except:
                info[item[1]] = element.text
            print((item[1],info[item[1]]))
    element = header_root.find('ModuleConfiguration')
    if element != None:
        config = ('ModuleConfiguration', element.attrib, element.text)
    if not enc:
        if (len(byte_values[header_tail_pos::]) % 4 == 0):
            print('Converting data to numpy array ...')
            raw_data = array.array('f', byte_values[header_tail_pos::])
            real = np.array(raw_data).reshape(len(raw_data)//2,2)
            comp = real[:,0] + 1.0j * real[:,1]
            if (len(comp) == info['SampleCount']):
                print('Done')
                data = comp
            else:
                print('Data length does not match with SampleCount!')
        else:
            print('Incorrect file length!')
    else:
        print('Data is encrypted! Cannot process.')
    return(data, info, config)

def iqvsa_read(filename):
    fh = open(filename,'rb')
    byte_values = fh.read()
    fh.close()
    wave, info, config = extract_data(byte_values)
    return(wave, info, config)

def decimate(sig, samplingrate=160000000, rate=16):
    # Decimate the samples by rate (default 16) so the entire capture can be shown in ms.
    length = len(sig) // rate
    t = np.linspace(0, length-1, length) / samplingrate * 1000
    return(t, sig[::rate])

def get_piece(start, stop, sig, samplingrate=160000000):
    # Carve out the piece in inside the capture specified by start and stop time in us
    if (stop > start):
        t1 = int(max(start * 1e-6 * samplingrate, 0))
        t2 = int(min(stop * 1e-6 * samplingrate, len(sig)))
        t = np.linspace(t1, t2, (t2-t1)) / samplingrate * 1000000
    else:
        print('Cannot Stop before Start!')
        t = 0
        wave = np.array([])
    return(t, sig[t1:t2])


if __name__ == "__main__":
    main()